package ro.tuc.ds2020;
import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.tools.json.JSONReader;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;


public class Producer {
    private final static String QUEUE_NAME = "PRODUCER";



    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost("roedeer.rmq.cloudamqp.com");
        factory.setVirtualHost("gjcbsqxv");
        factory.setUsername("gjcbsqxv");
        factory.setPassword("8QgfoCl9b7VrN_JPcMCMyVjAUBaXAb5c");
        factory.setPort(5672);

        List<String> draft = new ArrayList<String>();

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String path = "activity.txt";

            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);
            JSONObject g = new JSONObject();
            String line = br.readLine();

            while(line != null) {

                String mess =line;
                String[] arrOfStr = mess.split("\t\t", 3);

                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date parsedDate1 = dateFormat1.parse(arrOfStr[0]);
                Timestamp timestamp1 = new java.sql.Timestamp(parsedDate1.getTime());

                SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date parsedDate2 = dateFormat2.parse(arrOfStr[1]);
                Timestamp timestamp2 = new java.sql.Timestamp(parsedDate2.getTime());

                String id="81cdff70-8b8e-4823-94f4-19de7a25bb17"; //ID Pacient care este monitorizat

                g.put("id",id);
                g.put("activityName",arrOfStr[2]);
                g.put("startDate",timestamp1.getTime());
                g.put("endDate",timestamp2.getTime());



                channel.basicPublish("", QUEUE_NAME, null, g.toString().getBytes());
                line = br.readLine();
                System.out.println(g.toString());
                Thread.sleep(1000);
            }

            br.close();

        }


    }
}



